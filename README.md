## What is this
This is the SourceMod server plugin used in nadeking's video:
https://www.youtube.com/watch?v=-oTVq8IyVVk

## Files:
* SharedNadeDamage.smx = Already compiled plugin
* SharedNadeDamage.sp = SourceCode

## Known issues:
* Shared grenade & molotov damage do not always damage all teammates equally
* Grenade damage can sometimes be glitchy and instantly kill a player with full health
* Rare bug where the thrower of the grenade has died when his grenade damage an enemy

## How to compile:
###### This is not a tutorial, but you can Google: "SPEdit | SourcePawn Editor"

## How to install on my server:
###### This is not a tutorial. Learn about sourcemod at: https://wiki.alliedmods.net/Category:SourceMod_Documentation